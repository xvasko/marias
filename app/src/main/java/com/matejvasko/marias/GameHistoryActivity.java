package com.matejvasko.marias;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.nfc.Tag;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.matejvasko.marias.models.Game;
import com.matejvasko.marias.models.GameState;
import com.matejvasko.marias.utils.Utils;

import org.w3c.dom.Text;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

import static com.matejvasko.marias.MainActivity.realm;

public class GameHistoryActivity extends AppCompatActivity {

    RealmList<GameState> gameHistory;
    TableLayout tableLayout;

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(GameHistoryActivity.this);
            builder1.setTitle("Chcete vrátiť hru do " + view.getTag() + " kola?");
            builder1.setMessage("Všetky nasledujúce kolá budú vymazané.");
            builder1.setCancelable(true);
            builder1.setIcon(android.R.drawable.ic_dialog_alert);
            builder1.setPositiveButton(
                    "Ano",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            realm.deleteGameHistory(GameActivity.game.getName(),Integer.valueOf(view.getTag().toString()));
                            updateTable();
                            dialog.cancel();
                        }
                    });

            builder1.setNegativeButton(
                    "Nie",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_history);
        getSupportActionBar().hide();

        SharedPreferences sharedPref;
        sharedPref = getSharedPreferences("playerStarts", Context.MODE_PRIVATE);

        View view = findViewById(R.id.game_history_activity);
        view.setPadding(0, Utils.getStatusBarHeight(getResources()), 0 ,0);
        view.setBackgroundColor(ContextCompat.getColor(this, Utils.getCurrentBackground(getResources(), sharedPref)));

        gameHistory = realm.findGame(GameActivity.game.getName()).getGameHistory();

        tableLayout = findViewById(R.id.tableLayout);
        updateTable();
    }

    private void updateTable() {
        tableLayout.removeAllViews();
        tableLayout.addView(getHeaderRow(), new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));
        if (gameHistory.size() != 0) {
            for (int i = 0; i < gameHistory.size(); i++) {
                TableRow tableRow = getTableRow(String.valueOf(i + 1), gameHistory.get(i), i % 2);
                tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));
            }
        }
    }

    public TableRow getHeaderRow() {
        TableRow tableRow = new TableRow(this);
        tableRow.setBackgroundColor(getResources().getColor(R.color.cellDark));

        tableRow.addView(getHeaderCell("#", 0.3f));
        tableRow.addView(getHeaderCell("Hráč 1", 1.0f));
        tableRow.addView(getHeaderCell("Hráč 2", 1.0f));
        tableRow.addView(getHeaderCell("Hráč 3", 1.0f));
        if (GameActivity.game.isQuadrupleGame()) {
            tableRow.addView(getHeaderCell("Hráč 4", 1.0f));
        }
        return tableRow;
    }

    public TableRow getTableRow(String str, GameState gameState, int remainder) {
        TableRow tableRow = new TableRow(this);
        tableRow.setBackgroundColor(getResources().getColor(R.color.cellDark));

        tableRow.addView(getNumberCell(str, 0.3f, remainder));
        tableRow.addView(getCell(gameState.getPlayer1Amount(), 1.0f, remainder));
        tableRow.addView(getCell(gameState.getPlayer2Amount(), 1.0f, remainder));
        tableRow.addView(getCell(gameState.getPlayer3Amount(), 1.0f, remainder));
        if (GameActivity.game.isQuadrupleGame()) {
            tableRow.addView(getCell(gameState.getPlayer4Amount(), 1.0f, remainder));
        }

        return tableRow;
    }

    private TextView getHeaderCell(String str, float weight) {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.weight = weight;
        params.setMargins(1,1,1,1);

        TextView textView = new TextView(this);
        textView.setGravity(Gravity.CENTER);
        textView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        textView.setText(str);
        textView.setTextSize(24);
        textView.setPadding(0,6,0,6);
        textView.setLayoutParams(params);

        return textView;
    }

    private TextView getCell(String str, float weight, int remainder) {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.weight = weight;
        params.setMargins(1,1,1,1);
        TextView textView = new TextView(this);
        textView.setGravity(Gravity.CENTER);
        if (remainder == 1) {
            textView.setBackgroundColor(getResources().getColor(R.color.cellLight));
        } else {
            textView.setBackgroundColor(getResources().getColor(R.color.cellMediocre));
        }
        textView.setText(str);
        textView.setTextSize(20);
        textView.setPadding(0,6,0,6);
        textView.setLayoutParams(params);

        return textView;
    }

    private TextView getNumberCell(String str, float weight, int remainder) {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.weight = weight;
        params.setMargins(1,1,1,1);

        TextView textView = new TextView(this);
        textView.setGravity(Gravity.CENTER);
        if (remainder == 1) {
            textView.setBackgroundColor(getResources().getColor(R.color.cellLight));
        } else {
            textView.setBackgroundColor(getResources().getColor(R.color.cellMediocre));
        }
        textView.setText(str);
        textView.setTag(str);
        textView.setOnClickListener(onClickListener);
        textView.setTextSize(20);
        textView.setTextColor(getResources().getColor(R.color.colorAccent));
        textView.setPadding(0,6,0,6);
        textView.setLayoutParams(params);

        return textView;
    }

}
