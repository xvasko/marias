package com.matejvasko.marias.enums;

public enum Color {
    BLACK("Black"),
    BLUE("Blue"),
    RED("Red");

    private final String value;

    Color(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
