package com.matejvasko.marias.enums;

public enum GameStatus {
    CREATING("VYTVÁRA SA"),
    IN_PROGRESS("PREBIEHA");

    private final String value;

    GameStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
