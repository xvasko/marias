package com.matejvasko.marias.adapters;

public class CustomAdapterItem {

    public int icon;
    public String text1;
    public String text2;
    public String text3;

    public CustomAdapterItem(int icon, String text1, String text2, String text3) {
        this.icon = icon;
        this.text1 = text1;
        this.text2 = text2;
        this.text3 = text3;
    }

}
