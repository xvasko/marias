package com.matejvasko.marias.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.matejvasko.marias.R;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<CustomAdapterItem> {

    private Context context;
    private int layoutResourceId;
    private ArrayList<CustomAdapterItem> data;

    public CustomAdapter(Context context, int layoutResourceId, ArrayList<CustomAdapterItem> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CustomAdapterItemHolder holder;

        if(row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new CustomAdapterItemHolder();
            holder.imgIcon = row.findViewById(R.id.list_item_image_view);
            holder.txtTitle1 = row.findViewById(R.id.list_item_text1);
            holder.txtTitle2 = row.findViewById(R.id.list_item_text2);
            holder.txtTitle3 = row.findViewById(R.id.list_item_text3);

            row.setTag(holder);
        } else {
            holder = (CustomAdapterItemHolder) row.getTag();
        }

        CustomAdapterItem customAdapterItem = data.get(position);
        holder.imgIcon.setImageResource(customAdapterItem.icon);
        holder.txtTitle1.setText(customAdapterItem.text1);
        holder.txtTitle2.setText(customAdapterItem.text2);
        holder.txtTitle3.setText(customAdapterItem.text3);

        return row;
    }

    static class CustomAdapterItemHolder {
        ImageView imgIcon;
        TextView txtTitle1;
        TextView txtTitle2;
        TextView txtTitle3;
    }

}
