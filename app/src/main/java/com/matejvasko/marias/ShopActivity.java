package com.matejvasko.marias;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.Shape;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.matejvasko.marias.utils.Utils;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class ShopActivity extends AppCompatActivity {

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    TextView starsTextView;

    Set<String> openBackgrounds = new HashSet<>();
    int activeBackground;
    int stars;

    boolean avatar0Open;
    boolean avatar1Open;
    boolean avatar0Active;
    boolean avatar1Active;


    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        getSupportActionBar().hide();
        starsTextView = findViewById(R.id.textView);

        View view = findViewById(R.id.shop_activity_layout);
        view.setPadding(0, Utils.getStatusBarHeight(getResources()), 0 ,0);

        sharedPref = getSharedPreferences("playerStarts", Context.MODE_PRIVATE);
        editor     = sharedPref.edit();

//        editor.putStringSet("openBackgrounds", new HashSet<String>());
//        editor.putInt("activeBackground", findViewById(R.id.backgrounds_square_0_foreground).getId());
//        editor.putInt("stars", 1000);
//        editor.putBoolean("avatar0Active", false);
//        editor.putBoolean("avatar1Active", false);
//        editor.putBoolean("avatar0Open", false);
//        editor.putBoolean("avatar1Open", false);
//        editor.apply();

        refreshState();

        updateStars();
        updateBackgrounds();
        updateActiveBackground();
        updateAvatars();
        updateActiveAvatars();
    }

    public void activate(View view) {
        String tag = view.getTag().toString();

        if (!tag.equals("active")) {

            if (tag.equals("open")) {
                View activeView = findViewById(activeBackground);
                activeView.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.shop_item_layout_open));
                activeView.setTag("open");
                view.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.shop_item_layout_active));
                view.setTag("active");
                editor.putInt("activeBackground", view.getId());
                editor.apply();
                refreshState();
            } else {
                TextView priceTextView = ((TextView)((ViewGroup) view).getChildAt(1));
                int price = Integer.valueOf(priceTextView.getText().toString());

                if (sharedPref.getInt("stars",0) < price) {
                    Toast.makeText(getApplicationContext(),"Nemate dostatok hviezdiciek", Toast.LENGTH_LONG).show();
                } else {
                    setOpen(findViewById(activeBackground));

                    ((ViewGroup) view).getChildAt(0).setVisibility(View.INVISIBLE);
                    ((ViewGroup) view).getChildAt(1).setVisibility(View.INVISIBLE);

                    setActive(view);
                    editor.putInt("activeBackground", view.getId());

                    String idNum = getResources().getResourceEntryName(view.getId()).replaceAll("[^0-9]+", "");
                    Set<String> openBackgroundsLocal = new HashSet<>();
                    openBackgroundsLocal.addAll(openBackgrounds);
                    openBackgroundsLocal.add(idNum);

                    editor.putStringSet("openBackgrounds", openBackgroundsLocal);
                    editor.putInt("stars", stars - price);
                    editor.apply();
                    refreshState();
                    updateStars();
                }
            }
        }
    }

    public void toggleAvatar(View view) {
        String tag = view.getTag().toString();
        String idNum = getResources().getResourceEntryName(view.getId()).replaceAll("[^0-9]+", "");

        if (tag.equals("active")) {
            // DEACTIVATE
            setOpen(view);
            editor.putBoolean("avatar" + idNum + "Active", false);
            editor.apply();
            refreshState();
        } else {
            if (tag.equals("open")) {
                // ACTIVATE
                setActive(view);
                editor.putBoolean("avatar" + idNum + "Active", true);
                editor.apply();
                refreshState();
            } else {
                // BUY
                TextView priceTextView = ((TextView)((ViewGroup) view).getChildAt(1));
                int price = Integer.valueOf(priceTextView.getText().toString());

                if (sharedPref.getInt("stars",0) < price) {
                    Toast.makeText(getApplicationContext(),"Nemate dostatok hviezdiciek", Toast.LENGTH_LONG).show();
                } else {
                    ((ViewGroup) view).getChildAt(0).setVisibility(View.INVISIBLE);
                    ((ViewGroup) view).getChildAt(1).setVisibility(View.INVISIBLE);
                    setActive(view);

                    editor.putBoolean("avatar" + idNum + "Active", true);
                    editor.putBoolean("avatar" + idNum + "Open", true);
                    editor.putInt("stars", stars - price);
                    editor.apply();
                    refreshState();
                    updateStars();
                }
            }
        }
    }

    private void updateStars() {
        starsTextView.setText("Mate " + stars + " hviezdiciek");
    }

    private void updateActiveBackground() {
        setInitialActive(findViewById(activeBackground));
    }

    private void updateBackgrounds() {
        if (openBackgrounds.contains("1")) {
            setInitialOpen(findViewById(R.id.backgrounds_square_1_foreground));
        } if (openBackgrounds.contains("2")) {
            setInitialOpen(findViewById(R.id.backgrounds_square_2_foreground));
        } if (openBackgrounds.contains("3")) {
            setInitialOpen(findViewById(R.id.backgrounds_square_3_foreground));
        } if (openBackgrounds.contains("4")) {
            setInitialOpen(findViewById(R.id.backgrounds_square_4_foreground));
        } if (openBackgrounds.contains("5")) {
            setInitialOpen(findViewById(R.id.backgrounds_square_5_foreground));
        } if (openBackgrounds.contains("6")) {
            setInitialOpen(findViewById(R.id.backgrounds_square_6_foreground));
        } if (openBackgrounds.contains("7")) {
            setInitialOpen(findViewById(R.id.backgrounds_square_7_foreground));
        }
    }

    private void updateActiveAvatars() {
        if (avatar0Active) {
            setInitialActive(findViewById(R.id.avatars_square_0_foreground));
        } if (avatar1Active) {
            setInitialActive(findViewById(R.id.avatars_square_1_foreground));
        }
    }

    private void updateAvatars() {
        if (avatar0Open) {
            setInitialOpen(findViewById(R.id.avatars_square_0_foreground));
        } if (avatar1Open) {
            setInitialOpen(findViewById(R.id.avatars_square_1_foreground));
        }
    }

    private void setActive(View view) {
        view.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.shop_item_layout_active));
        view.setTag("active");
    }

    private void setOpen(View view) {
        view.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.shop_item_layout_open));
        view.setTag("open");
    }

    private void setInitialActive(View view) {
        view.setTag("active");
        view.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.shop_item_layout_active));
    }

    private void setInitialOpen(View view) {
        ((ViewGroup) view).getChildAt(0).setVisibility(View.INVISIBLE);
        ((ViewGroup) view).getChildAt(1).setVisibility(View.INVISIBLE);
        view.setTag("open");
        view.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.shop_item_layout_open));
    }

    private void refreshState() {
        openBackgrounds  = sharedPref.getStringSet("openBackgrounds", new HashSet<String>());
        activeBackground = sharedPref.getInt("activeBackground", findViewById(R.id.backgrounds_square_0_foreground).getId());
        avatar0Active    = sharedPref.getBoolean("avatar0Active", false);
        avatar1Active    = sharedPref.getBoolean("avatar1Active", false);
        avatar0Open      = sharedPref.getBoolean("avatar0Open", false);
        avatar1Open      = sharedPref.getBoolean("avatar1Open", false);
        stars            = sharedPref.getInt("stars", 0);
    }

}
