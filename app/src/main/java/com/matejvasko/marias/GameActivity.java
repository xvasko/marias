package com.matejvasko.marias;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.matejvasko.marias.enums.Color;
import com.matejvasko.marias.enums.GameStatus;
import com.matejvasko.marias.models.Game;
import com.matejvasko.marias.models.Player;
import com.matejvasko.marias.utils.CircularArrayList;
import com.matejvasko.marias.utils.Utils;

import java.math.BigDecimal;

import static com.matejvasko.marias.MainActivity.realm;

public class GameActivity extends AppCompatActivity implements RewardedVideoAdListener {

    CircularArrayList<Player> players = new CircularArrayList<>();
    String[] playersColorBackup = new String[4];
    static Game game;

    Button    buttonDividePool;
    Button    okButton;
    ImageView player1Icon;
    ImageView player2Icon;
    ImageView player3Icon;
    ImageView player4Icon;
    TextView poolTextView;
    TextView player1Amount;
    TextView player2Amount;
    TextView player3Amount;
    TextView player4Amount;
    TextView gameTableTextViewRow0Column1;
    TextView gameTableTextViewRow0Column3;
    TextView gameTableTextViewRow1Column1;
    TextView gameTableTextViewRow1Column3;
    TextView gameTableTextViewRow2Column1;
    TextView gameTableTextViewRow2Column3;
    TextView gameTableTextViewRow3Column2;
    TextView gameTableTextViewRow4Column2;
    TextView divisionTextView;
    View gameTable;
    ImageView timesTwo;
    ImageView timesTwoUndo;
    ImageView historyImageView;
    TextView gameNameTextView;

    BigDecimal pool = ZERO;

    final static BigDecimal TWO     = new BigDecimal("2");
    final static BigDecimal ZERO    = new BigDecimal("0.00");
    final static BigDecimal amount1 = new BigDecimal("0.01");
    final static BigDecimal amount2 = new BigDecimal("0.02");
    final static BigDecimal amount3 = new BigDecimal("0.04");
    final static BigDecimal amount4 = new BigDecimal("0.08");
    final static BigDecimal amount5 = new BigDecimal("0.15");
    final static BigDecimal amount6 = new BigDecimal("0.30");

    boolean gameSilentActive = false;
    boolean gameLoudActive = false;
    boolean sevenSilentActive = false;
    boolean sevenLoudActive = false;
    boolean hundredSilentActive = false;
    boolean hundredLoudActive = false;
    boolean battleActive = false;
    boolean durchActive = false;
    boolean battleDurchSelection = false;

    boolean divisionActive = false;
    boolean selectingPlayers = true;

    int multiplicationCounter = 0;
    int redAvatar;
    int blueAvatar;

    private AdView mAdView;
    RewardedVideoAd mRewardedVideoAd;
    ImageView adButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        getSupportActionBar().hide();

        SharedPreferences sharedPref;
        sharedPref = getSharedPreferences("playerStarts", Context.MODE_PRIVATE);

        if (sharedPref.getBoolean("avatar0Active", false)) {
            redAvatar = R.drawable.person_red_icon_mustache;
        } else {
            redAvatar = R.drawable.person_red_icon;
        }

        if (sharedPref.getBoolean("avatar1Active", false)) {
            blueAvatar = R.drawable.person_blue_icon_cigare;
        } else {
            blueAvatar = R.drawable.person_blue_icon;
        }

        View view = findViewById(R.id.relativeLayout);
        view.setPadding(0, Utils.getStatusBarHeight(getResources()), 0 ,0);
        view.setBackgroundColor(ContextCompat.getColor(this, Utils.getCurrentBackground(getResources(), sharedPref)));

        game = realm.findGame(getIntent().getStringExtra("game name"));

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.loadAd("ca-app-pub-9861673834715515/7251921524", adRequest);
        mRewardedVideoAd.setRewardedVideoAdListener(this);

        adButton = findViewById(R.id.ad_button);
        adButton.setEnabled(false);

        adButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adButton.setEnabled(false);
                adButton.setImageResource(R.drawable.star_disabled_icon);
                if (mRewardedVideoAd.isLoaded()) {
                    mRewardedVideoAd.show();
                } else {
                    System.out.println("add is not loaded");
                }
            }
        });

        if (!game.isQuadrupleGame()) {
            TextView selectBlackTextView = findViewById(R.id.selectBlackTextView);
            selectBlackTextView.setText("Kto bude začínať?");
        }

        prepareUI();
        createPlayers();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        adButton.setEnabled(true);
        adButton.setImageResource(R.drawable.star_plus_icon);
        Log.i("MainActivity", "An ad has loaded.");
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.i("MainActivity", "An ad has opend.");
    }

    @Override
    public void onRewardedVideoStarted() {
        Log.i("MainActivity", "An ad has started.");
    }

    @Override
    public void onRewardedVideoAdClosed() {
        mRewardedVideoAd.loadAd("ca-app-pub-9861673834715515/7251921524", new AdRequest.Builder().build());
        Log.i("MainActivity", "An ad has closed.");
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        SharedPreferences sharedPref = getSharedPreferences("playerStarts", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("stars", sharedPref.getInt("stars", 0) + rewardItem.getAmount());
        editor.apply();
        Toast.makeText(this, String.format("Ziskali ste %d hviezdiciek!", rewardItem.getAmount()), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Log.i("MainActivity", "An ad has caused focus to leave.");
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        Log.i("MainActivity", "An ad has failed to load.");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (game.getStatus().equals(GameStatus.IN_PROGRESS.getValue())) {
            continueGame();
        }
        updatePlayers();
        updatePlayersAmount();
        updateGameTitle();
    }

    private void continueGame() {
        selectingPlayers = false;
        findViewById(R.id.middlePlayerSelection).setVisibility(View.INVISIBLE);
        findViewById(R.id.middleGameInProgress).setVisibility(View.VISIBLE);
        findViewById(R.id.okButton).setVisibility(View.INVISIBLE);

        // get current initial player
        if (game.isQuadrupleGame()) {
            int[] pointerArray = { 0, 1, 2, 3 };
            int pointer = game.getInitialPlayer() - (game.getGameHistory().size() % 4);
            if (pointer < 0) {
                pointer += 4;
            }
            selectPlayersColors(pointerArray[pointer]);
        } else {
            int[] pointerArray = { 0, 1, 2 };
            int pointer = game.getInitialPlayer() - (game.getGameHistory().size() % 3);
            if (pointer < 0) {
                pointer += 3;
            }
            selectPlayersColors(pointerArray[pointer]);
        }

    }

    private void prepareUI() {
        okButton                     = findViewById(R.id.okButton);
        poolTextView                 = findViewById(R.id.poolTextView);
        divisionTextView             = findViewById(R.id.division_text_view);
        buttonDividePool             = findViewById(R.id.buttonDividePool);
        player1Icon                  = findViewById(R.id.player1Icon);
        player2Icon                  = findViewById(R.id.player2Icon);
        player3Icon                  = findViewById(R.id.player3Icon);
        player4Icon                  = findViewById(R.id.player4Icon);
        player1Amount                = findViewById(R.id.player1Amount);
        player2Amount                = findViewById(R.id.player2Amount);
        player3Amount                = findViewById(R.id.player3Amount);
        player4Amount                = findViewById(R.id.player4Amount);
        gameTableTextViewRow0Column1 = findViewById(R.id.gameTableTextViewRow0Column1);
        gameTableTextViewRow0Column3 = findViewById(R.id.gameTableTextViewRow0Column3);
        gameTableTextViewRow1Column1 = findViewById(R.id.gameTableTextViewRow1Column1);
        gameTableTextViewRow1Column3 = findViewById(R.id.gameTableTextViewRow1Column3);
        gameTableTextViewRow2Column1 = findViewById(R.id.gameTableTextViewRow2Column1);
        gameTableTextViewRow2Column3 = findViewById(R.id.gameTableTextViewRow2Column3);
        gameTableTextViewRow3Column2 = findViewById(R.id.gameTableTextViewRow3Column2);
        gameTableTextViewRow4Column2 = findViewById(R.id.gameTableTextViewRow4Column2);
        gameTable                    = findViewById(R.id.gameTable);
        timesTwo                     = findViewById(R.id.timesTwo);
        timesTwoUndo                 = findViewById(R.id.timesTwoUndo);
        historyImageView             = findViewById(R.id.history_image_view);
        gameNameTextView             = findViewById(R.id.gameNameTextView);
    }

    private void createPlayers() {
        Player player1 = new Player();
        Player player2 = new Player();
        Player player3 = new Player();
        players.add(player1);
        players.add(player2);
        players.add(player3);
        if (game.isQuadrupleGame()) {
            Player player4 = new Player();
            players.add(player4);
            findViewById(R.id.player4Layout).setVisibility(View.VISIBLE);
        }
    }

    private void updatePlayers() {
        if (game.getGameHistory().size() != 0) {
            players.get(0).setAmount(new BigDecimal(game.getGameHistory().get(game.getGameHistory().size() - 1 ).getPlayer1Amount()));
            players.get(1).setAmount(new BigDecimal(game.getGameHistory().get(game.getGameHistory().size() - 1 ).getPlayer2Amount()));
            players.get(2).setAmount(new BigDecimal(game.getGameHistory().get(game.getGameHistory().size() - 1 ).getPlayer3Amount()));
            if (game.isQuadrupleGame()) {
                players.get(3).setAmount(new BigDecimal(game.getGameHistory().get(game.getGameHistory().size() - 1 ).getPlayer4Amount()));
                findViewById(R.id.player4Layout).setVisibility(View.VISIBLE);
            }
        }
    }

    public void playerClick(View view) {
        int i = Integer.valueOf(view.getTag().toString());
        if (selectingPlayers) {
            if (!okButton.isEnabled()) okButton.setEnabled(true);
            selectPlayersColors(i);
        }
        if (divisionActive) {
            if (players.get(i).getColor().equals("Black")) return;
            if (battleActive || durchActive) {
                selectPlayerPlayingBattleDurch(i);
            } else {
                dividePool(i);
                updatePlayersAmount();
            }
        }
    }

    private void selectPlayerPlayingBattleDurch(int i) {
        if (battleDurchSelection) {
            dividePool(i);
            // set remembered players colors
            for (int j = 0; j < players.size(); j++) {
                players.get(j).setColor(playersColorBackup[j]);
            }
            movePlayers();
            updatePlayersAmount();
            battleDurchSelection = false;
        } else {
            // remember players colors
            for (int j = 0; j < players.size(); j++) {
                playersColorBackup[j] = players.get(j).getColor();
            }

            if (players.get(i).getColor().equals("Red")) {
                for (int j = 0; j < players.size(); j++) {
                    if (players.get(j).getColor().equals("Red")) {
                        if (i == j) {
                            players.get(j).setColor("Blue");
                        }
                    } else if (players.get(j).getColor().equals("Blue")) {
                        players.get(j).setColor("Red");
                    }
                }
                updatePlayersColor();
            }
            if (durchActive) {
                divisionTextView.setText("Kto vyhral Durch?");
            } else {
                divisionTextView.setText("Kto vyhral Battle?");
            }
            battleDurchSelection = true;
        }

    }

    private void selectPlayersColors(int i) {
        if (game.isQuadrupleGame()) {
            players.get(i).setColor(Color.BLACK.getValue());
            players.get(i + 1).setColor(Color.RED.getValue());
            players.get(i + 2).setColor(Color.RED.getValue());
            players.get(i + 3).setColor(Color.BLUE.getValue());
        } else {
            players.get(i).setColor(Color.BLUE.getValue());
            players.get(i + 1).setColor(Color.RED.getValue());
            players.get(i + 2).setColor(Color.RED.getValue());
        }

        updatePlayersColor();
    }

    private void updatePlayersColor() {
        for (int j = 0; j < players.size(); j++ ) {
            ImageView imageView = null;
            switch (j) {
                case 0:
                    imageView = player1Icon;
                    break;
                case 1:
                    imageView = player2Icon;
                    break;
                case 2:
                    imageView = player3Icon;
                    break;
                case 3:
                    imageView = player4Icon;
                    break;
                default:
                    break;
            }
            Player player = players.get(j);
            if (player.getColor().equals(Color.BLACK.getValue())) {
                if (battleActive || durchActive) {
                    imageView.setImageResource(R.drawable.person_icon_denied);
                } else {
                    imageView.setImageResource(R.drawable.person_icon);
                }
            }
            if (player.getColor().equals(Color.RED.getValue())) {
                imageView.setImageResource(redAvatar);
            }
            if (player.getColor().equals(Color.BLUE.getValue())) {
                imageView.setImageResource(blueAvatar);
            }
        }
    }

    private void dividePool(int i) {
        BigDecimal multiplier;

        if (game.isQuadrupleGame()) {
            multiplier = new BigDecimal("3");
        } else {
            multiplier = new BigDecimal("2");
        }

        Player player;

        for (int j = 0; j < players.size(); j++) {
            player = players.get(j);
            // if Blue won
            if (players.get(i).getColor().equals("Blue")) {
                if (player.getColor().equals("Blue")) {
                    player.setAmount(player.getAmount().add(pool.multiply(multiplier)));
                } else {
                    player.setAmount(player.getAmount().subtract(pool));
                }
            } else {
                if (player.getColor().equals("Blue")) {
                    player.setAmount(player.getAmount().subtract(pool.multiply(multiplier)));
                } else {
                    player.setAmount(player.getAmount().add(pool));
                }
            }
        }

        finishPoolDivision();

    }

    public void okButtonClick(View view) {
        selectingPlayers = false;
        findViewById(R.id.middlePlayerSelection).setVisibility(View.INVISIBLE);
        findViewById(R.id.middleGameInProgress).setVisibility(View.VISIBLE);
        findViewById(R.id.okButton).setVisibility(View.INVISIBLE);

        realm.updateGameStatus(game, players);
    }

    public String addEuroSign(String value) {
        return value + "€";
    }

    private void updatePoolTextView() {
        poolTextView.setText(addEuroSign(pool.toString()));
    }

    private void updatePlayersAmount() {
        player1Amount.setText(addEuroSign(players.get(0).getAmount().toString()));
        player2Amount.setText(addEuroSign(players.get(1).getAmount().toString()));
        player3Amount.setText(addEuroSign(players.get(2).getAmount().toString()));
        if (game.isQuadrupleGame()) {
            player4Amount.setText(addEuroSign(players.get(3).getAmount().toString()));
        }
    }

    public void dividePool(View view) {
        divisionActive = true;
        toggleDivisionUI();
    }

    private void finishPoolDivision() {
        divisionActive = false;
        toggleDivisionUI();
        realm.persistGameState(game, players);
        resetTable();
        movePlayers();
        updateGameTitle();
    }

    private void toggleDivisionUI() {
        float alpha;
        int blackPersonImg;

        if (divisionActive) {
            alpha = 0.5f;
            blackPersonImg = R.drawable.person_icon_denied;

            divisionTextView.setVisibility(View.VISIBLE);
            gameTable.setVisibility(View.INVISIBLE);
            timesTwo.setImageResource(R.drawable.cancel_icon);
            timesTwoUndo.setVisibility(View.INVISIBLE);
            buttonDividePool.setVisibility(View.INVISIBLE);
            historyImageView.setVisibility(View.INVISIBLE);

            if (battleActive) {
                divisionTextView.setText("Kto to zobral na Battle?");
            } else if (durchActive){
                divisionTextView.setText("Kto to zobral na Durch?");
            } else {
                divisionTextView.setText("Kto vyhral?");
            }
        } else {
            alpha = 1f;
            blackPersonImg = R.drawable.person_icon;

            divisionTextView.setVisibility(View.INVISIBLE);
            gameTable.setVisibility(View.VISIBLE);
            timesTwo.setImageResource(R.drawable.x_icon);
            timesTwoUndo.setVisibility(View.VISIBLE);
            buttonDividePool.setVisibility(View.VISIBLE);
            historyImageView.setVisibility(View.VISIBLE);
        }

        for (int j = 0; j < players.size(); j++ ) {
            ImageView imageView = null;
            switch (j) {
                case 0:
                    imageView = player1Icon;
                    break;
                case 1:
                    imageView = player2Icon;
                    break;
                case 2:
                    imageView = player3Icon;
                    break;
                case 3:
                    imageView = player4Icon;
                    break;
                default:
                    break;
            }

            Player player = players.get(j);
            if (player.getColor().equals(Color.BLACK.getValue())) {
                imageView.setImageResource(blackPersonImg);
            }
            if (player.getColor().equals(Color.RED.getValue())) {
                imageView.setAlpha(alpha);
            }
            if (player.getColor().equals(Color.BLUE.getValue())) {
                imageView.setAlpha(alpha);
            }
        }
    }

    private void resetTable() {
        pool = ZERO;
        updatePoolTextView();
        multiplicationCounter = 0;
        gameSilentActive = false;
        gameLoudActive = false;
        sevenSilentActive = false;
        sevenLoudActive = false;
        hundredSilentActive = false;
        hundredLoudActive = false;
        battleActive = false;
        durchActive = false;
        gameTableTextViewRow0Column1.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow0Column1.setTextColor(getResources().getColor(R.color.black));
        gameTableTextViewRow0Column3.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow0Column3.setTextColor(getResources().getColor(R.color.black));
        gameTableTextViewRow1Column1.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow1Column1.setTextColor(getResources().getColor(R.color.black));
        gameTableTextViewRow1Column3.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow1Column3.setTextColor(getResources().getColor(R.color.black));
        gameTableTextViewRow2Column1.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow2Column1.setTextColor(getResources().getColor(R.color.black));
        gameTableTextViewRow2Column3.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow2Column3.setTextColor(getResources().getColor(R.color.black));
        gameTableTextViewRow3Column2.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow3Column2.setTextColor(getResources().getColor(R.color.black));
        gameTableTextViewRow4Column2.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow4Column2.setTextColor(getResources().getColor(R.color.black));
    }

    private void movePlayers() {
        if (!(battleActive || durchActive)) {
            String firstColor = players.get(0).getColor();
            for (int j = 0; j < players.size(); j++) {
                if (j == players.size() - 1) {
                    players.get(j).setColor(firstColor);
                } else {
                    players.get(j).setColor(players.get(j + 1).getColor());
                }
            }
            updatePlayersColor();
        }
    }

    public void historyClick(View view) {
        Intent intent = new Intent(this, GameHistoryActivity.class);
        startActivity(intent);
    }

    /**
     * ROW 0
     */

    public void gameTableTextViewRow0Column1Click(View view) {
        if (divisionActive) return;
        if (gameSilentActive) {
            disableSilentGame();
        } else {
            enableSilentGame();
            if (gameLoudActive) {
                disableLoudGame();
            }
        }
        updatePoolTextView();
    }

    public void gameTableTextViewRow0Column2Click(View view) {
        if (divisionActive) return;
        if (gameLoudActive) {
            disableLoudGame();
        } else {
            enableLoudGame();
            if (gameSilentActive) {
                disableSilentGame();
            }
        }
        updatePoolTextView();
    }

    private void enableSilentGame() {
        if (battleActive || durchActive || multiplicationCounter > 0) resetTable();
        gameTableTextViewRow0Column1.setBackgroundColor(getResources().getColor(R.color.cellDark));
        gameTableTextViewRow0Column1.setTextColor(getResources().getColor(R.color.cellLight));
        pool = pool.add(amount1);
        gameSilentActive = true;
    }

    private void disableSilentGame() {
        if (battleActive || durchActive || multiplicationCounter > 0) {
            resetTable();
        } else {
            pool = pool.subtract(amount1);
        }
        gameTableTextViewRow0Column1.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow0Column1.setTextColor(getResources().getColor(R.color.black));
        gameSilentActive = false;
    }

    private void enableLoudGame() {
        if (battleActive || durchActive || multiplicationCounter > 0) resetTable();
        gameTableTextViewRow0Column3.setBackgroundColor(getResources().getColor(R.color.cellDark));
        gameTableTextViewRow0Column3.setTextColor(getResources().getColor(R.color.cellLight));
        pool = pool.add(amount2);
        gameLoudActive = true;
    }

    private void disableLoudGame() {
        if (battleActive || durchActive || multiplicationCounter > 0) {
            resetTable();
        } else {
            pool = pool.subtract(amount2);
        }
        gameTableTextViewRow0Column3.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow0Column3.setTextColor(getResources().getColor(R.color.black));
        gameLoudActive = false;
    }

    /**
     * ROW 1
     */
    public void gameTableTextViewRow1Column1Click(View view) {
        if (divisionActive) return;
        if (sevenSilentActive) {
            disableSilentSeven();
        } else {
            enableSilentSeven();
            if (sevenLoudActive) {
                disableLoudSeven();
            }
        }
        updatePoolTextView();
    }

    public void gameTableTextViewRow1Column3Click(View view) {
        if (divisionActive) return;
        if (sevenLoudActive) {
            disableLoudSeven();
        } else {
            enableLoudSeven();
            if (sevenSilentActive) {
                disableSilentSeven();
            }
        }
        updatePoolTextView();
    }

    private void enableSilentSeven() {
        if (battleActive || durchActive || multiplicationCounter > 0) resetTable();
        gameTableTextViewRow1Column1.setBackgroundColor(getResources().getColor(R.color.cellDark));
        gameTableTextViewRow1Column1.setTextColor(getResources().getColor(R.color.cellLight));
        pool = pool.add(amount2);
        sevenSilentActive = true;
    }

    private void disableSilentSeven() {
        if (battleActive || durchActive || multiplicationCounter > 0) {
            resetTable();
        } else {
            pool = pool.subtract(amount2);
        }
        gameTableTextViewRow1Column1.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow1Column1.setTextColor(getResources().getColor(R.color.black));
        sevenSilentActive = false;
    }

    private void enableLoudSeven() {
        if (battleActive || durchActive || multiplicationCounter > 0) resetTable();
        gameTableTextViewRow1Column3.setBackgroundColor(getResources().getColor(R.color.cellDark));
        gameTableTextViewRow1Column3.setTextColor(getResources().getColor(R.color.cellLight));
        pool = pool.add(amount3);
        sevenLoudActive = true;
    }

    private void disableLoudSeven() {
        if (battleActive || durchActive || multiplicationCounter > 0) {
            resetTable();
        } else {
            pool = pool.subtract(amount3);
        }
        gameTableTextViewRow1Column3.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow1Column3.setTextColor(getResources().getColor(R.color.black));
        sevenLoudActive = false;
    }

    /**
     * ROW 2
     */
    public void gameTableTextViewRow2Column1Click(View view) {
        if (divisionActive) return;
        if (hundredSilentActive) {
            disableSilentHundred();
        } else {
            enableSilentHundred();
            if (hundredLoudActive) {
                disableLoudHundred();
            }
        }
        updatePoolTextView();
    }

    public void gameTableTextViewRow2Column3Click(View view) {
        if (divisionActive) return;
        if (hundredLoudActive) {
            disableLoudHundred();
        } else {
            enableLoudHundred();
            if (hundredSilentActive) {
                disableSilentHundred();
            }
        }
        updatePoolTextView();
    }

    private void enableSilentHundred() {
        if (battleActive || durchActive || multiplicationCounter > 0) resetTable();
        gameTableTextViewRow2Column1.setBackgroundColor(getResources().getColor(R.color.cellDark));
        gameTableTextViewRow2Column1.setTextColor(getResources().getColor(R.color.cellLight));
        pool = pool.add(amount3);
        hundredSilentActive = true;
    }

    private void disableSilentHundred() {
        if (battleActive || durchActive || multiplicationCounter > 0) {
            resetTable();
        } else {
            pool = pool.subtract(amount3);
        }
        gameTableTextViewRow2Column1.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow2Column1.setTextColor(getResources().getColor(R.color.black));
        hundredSilentActive = false;
    }

    private void enableLoudHundred() {
        if (battleActive || durchActive || multiplicationCounter > 0) resetTable();
        gameTableTextViewRow2Column3.setBackgroundColor(getResources().getColor(R.color.cellDark));
        gameTableTextViewRow2Column3.setTextColor(getResources().getColor(R.color.cellLight));
        pool = pool.add(amount4);
        hundredLoudActive = true;
    }

    private void disableLoudHundred() {
        if (battleActive || durchActive || multiplicationCounter > 0){
            resetTable();
        } else {
            pool = pool.subtract(amount4);
        }
        gameTableTextViewRow2Column3.setBackgroundColor(getResources().getColor(R.color.cellLight));
        gameTableTextViewRow2Column3.setTextColor(getResources().getColor(R.color.black));
        hundredLoudActive = false;
    }

    /**
     * Row 3 Battle
     */
    public void gameTableTextViewRow3Column2Click(View view) {
        if (divisionActive) return;
        if (battleActive) {
            resetTable();
        } else {
            enableBattle();
        }
        updatePoolTextView();
    }

    private void enableBattle() {
        resetTable();
        gameTableTextViewRow3Column2.setBackgroundColor(getResources().getColor(R.color.cellDark));
        gameTableTextViewRow3Column2.setTextColor(getResources().getColor(R.color.cellLight));
        pool = pool.add(amount5);
        battleActive = true;
    }

    /**
     * Row 4 Durch
     */
    public void gameTableTextViewRow4Column2Click(View view) {
        if (divisionActive) return;
        if (durchActive) {
            resetTable();
        } else {
            enableDurch();
        }
        updatePoolTextView();
    }

    private void enableDurch() {
        resetTable();
        gameTableTextViewRow4Column2.setBackgroundColor(getResources().getColor(R.color.cellDark));
        gameTableTextViewRow4Column2.setTextColor(getResources().getColor(R.color.cellLight));
        pool = pool.add(amount6);
        durchActive = true;
    }

    public void timesTwoClick(View view) {
        if (divisionActive) {
            divisionActive = false;
            if (battleDurchSelection) {
                // set remembered players colors
                for (int j = 0; j < players.size(); j++) {
                    players.get(j).setColor(playersColorBackup[j]);
                }
                updatePlayersColor();
                battleDurchSelection = false;
            }
            toggleDivisionUI();
            resetTable();
        } else {
            if (multiplicationCounter > 9) return;
            multiplicationCounter += 1;
            pool = pool.multiply(TWO);
            updatePoolTextView();
        }
    }

    public void timesTwoUndoClick(View view) {
        if (divisionActive) return;
        if (!(multiplicationCounter <= 0)) {
            multiplicationCounter -= 1;
            pool = pool.divide(TWO);
            updatePoolTextView();
        }
    }

    private void updateGameTitle() {
        gameNameTextView.setText(game.getName() + " - " + (game.getGameHistory().size() + 1) + " kolo");
    }

}
