package com.matejvasko.marias;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.matejvasko.marias.adapters.CustomAdapter;
import com.matejvasko.marias.adapters.CustomAdapterItem;
import com.matejvasko.marias.enums.GameStatus;
import com.matejvasko.marias.models.Game;
import com.matejvasko.marias.utils.GameNameRepository;
import com.matejvasko.marias.utils.RealmUtils;
import com.matejvasko.marias.utils.Utils;

import java.util.ArrayList;

import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    static RealmUtils realm;
    Button startGameButton;
    TextInputLayout gameNameWrapper;
    ListView gamesListView;
    RadioButton radio3Players;
    RadioButton radio4Players;
    ArrayList<CustomAdapterItem> items;
    ArrayList<CustomAdapterItem> deletionQueue = new ArrayList<>();
    boolean deletionInProcess = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        View view = findViewById(R.id.main_activity_layout);
        view.setPadding(0, Utils.getStatusBarHeight(getResources()), 0 ,0);

        MobileAds.initialize(this, "ca-app-pub-9861673834715515~4007165339");

        realm = new RealmUtils(getApplicationContext());
        prepareUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateListView();
    }

    public void prepareUI() {
        gameNameWrapper = findViewById(R.id.gameNameWrapper);
        gameNameWrapper.setHint("Zadajte názov hry");
        startGameButton = findViewById(R.id.startGameButton);
        gamesListView = findViewById(R.id.gamesListView);
        radio3Players = findViewById(R.id.radio_3_players);
        radio4Players = findViewById(R.id.radio_4_players);
    }

    private boolean validateGameNameFormat(String gameName) {
        return gameName.length() > 2;
    }

    private boolean validateGameNameAvailability(String gameName) {
        return realm.findGame(gameName) == null;
    }

    private void updateListView() {
        RealmResults<Game> result = realm.findAllGames();

        if (result.isEmpty()) {

        }

        deletionQueue.clear();
        items = new ArrayList<>();

        for (Game game : result) {
            int iconResource = R.drawable.person_3_icon;
            if (game.isQuadrupleGame()) {
                iconResource = R.drawable.person_4_icon;
            }
            CustomAdapterItem item = new CustomAdapterItem(iconResource, game.getName(), game.getDate(), game.getStatus());
            items.add(item);
        }

        final CustomAdapter adapter = new CustomAdapter(this, R.layout.list_item, items);
        final SwipeMenuListView listView = findViewById(R.id.gamesListView);

        listView.setAdapter(adapter);

        // Delete button swipe
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9, 0x3F, 0x25)));
                deleteItem.setWidth(220);
                deleteItem.setIcon(R.drawable.ic_delete);
                menu.addMenuItem(deleteItem);
            }
        };

        listView.setMenuCreator(creator);

        // on Delete button click
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                deletionQueue.add(items.remove(position));
                deletionInProcess = true;
                Animation fadeOut = new AlphaAnimation(1, 0);
                fadeOut.setDuration(350);
                final View listItem = listView.getChildAt(listView.getLastVisiblePosition() - listView.getFirstVisiblePosition());
                listItem.startAnimation(fadeOut);
                Handler handle = new Handler();
                handle.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        listItem.setVisibility(View.INVISIBLE);
                    }
                }, 350);
                handle.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        listItem.setVisibility(View.VISIBLE);
                        deletionInProcess = false;
                        Snackbar snackbar = Snackbar.make(gamesListView, "Game deleted", Snackbar.LENGTH_SHORT)
                            .setAction("UNDO", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    // nothing
                                }
                            })
                            .addCallback(new Snackbar.Callback() {
                                @Override
                                public void onDismissed(Snackbar transientBottomBar, int event) {
                                    super.onDismissed(transientBottomBar, event);
                                    switch (event) {
                                        case Snackbar.Callback.DISMISS_EVENT_TIMEOUT:
                                            realm.deleteGame(deletionQueue.get(0).text1);
                                            deletionQueue.remove(0);
                                            System.out.println("DISMISS_EVENT_TIMEOUT");
                                            break;
                                        case Snackbar.Callback.DISMISS_EVENT_SWIPE:
                                            realm.deleteGame(deletionQueue.get(0).text1);
                                            deletionQueue.remove(0);
                                            System.out.println("DISMISS_EVENT_SWIPE");
                                            break;
                                        case Snackbar.Callback.DISMISS_EVENT_CONSECUTIVE:
                                            if (deletionQueue.size() != 1) {
                                                realm.deleteGame(deletionQueue.get(0).text1);
                                                deletionQueue.remove(0);
                                            }
                                            System.out.println("DISMISS_EVENT_CONSECUTIVE");
                                            break;
                                        case Snackbar.Callback.DISMISS_EVENT_ACTION:
                                            items.add(deletionQueue.get(deletionQueue.size() - 1));
                                            adapter.notifyDataSetChanged();
                                            System.out.println("DISMISS_EVENT_ACTION");
                                            break;
                                        default:
                                            System.out.println("DO NOTHING");
                                            break;
                                    }
                                }
                            });
                        snackbar.show();
                    }
                }, 450);

                // false : close the menu; true : not close the menu
                return false;
            }
        });

        // on List Item click
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (!deletionInProcess) {
                    if (realm.findGame(items.get(i).text1) != null) {
                        Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                        intent.putExtra("game name", items.get(i).text1);
                        intent.putExtra("game status", items.get(i).text2);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Game " + items.get(i).text1 + " no longer exists", Toast.LENGTH_LONG).show();
                        items.remove(i);
                        adapter.notifyDataSetChanged();
                    }

                }
            }
        });
    }

    public void startGame(View view) {
        String gameName = gameNameWrapper.getEditText().getText().toString();
        if (!validateGameNameFormat(gameName)) {
            gameNameWrapper.setError("Zadajte aspoň 3 znaky");
        } else if (!validateGameNameAvailability(gameName))  {
            gameNameWrapper.setError("Hra s týmto názvom už existuje");
        } else {
            hideKeyboard();
            boolean isQuadrupleGame;
            if (radio4Players.isChecked()) {
                isQuadrupleGame = true;
            } else {
                isQuadrupleGame = false;
            }
            Intent intent = new Intent(this, GameActivity.class);

            intent.putExtra("game name", gameName);
            intent.putExtra("game status", GameStatus.CREATING.getValue());
            startActivity(intent);

            gameNameWrapper.getEditText().setText(null);
            gameNameWrapper.setError(null);
            gameNameWrapper.getEditText().clearFocus();

            realm.createGame(gameName, isQuadrupleGame);
        }
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void diceIconClick(View view) {
        gameNameWrapper.getEditText().setText(GameNameRepository.generateRandomGameName());
    }

    public void trolleyIconClick(View view) {
        Intent intent = new Intent(this, ShopActivity.class);
        startActivity(intent);
    }

}
