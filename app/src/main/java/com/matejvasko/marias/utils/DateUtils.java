package com.matejvasko.marias.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {

    public static String getDate() {
        DateFormat df = new SimpleDateFormat("d.M.yyyy HH:mm");
        df.setTimeZone(TimeZone.getDefault());
        return df.format(new Date());
    }

}
