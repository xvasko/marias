package com.matejvasko.marias.utils;

import java.util.Random;

public class GameNameRepository {

    private static final String[] maleAndjectives = new String[] {"Bezdrôtový", "Plesnivý", "Hriešny", "Krvavý", "Zafajčený", "Posvätný", "Ortuťový", "Železný", "Tichý", "Hlasný", "Hrdzavý", "Rakúsko-Uhorský", "Stimulujúci", "Mazľavý", "Srdcový", "Žaluďový", "Pikový", "Listový", "Kartový"};
    private static final String[] maleNouns       = new String[] {"Stôl", "Battle", "Durch", "Pajzel", "Šenk", "Duel", "Zápas"};

    public GameNameRepository(RealmUtils realm) {
    }

    public static String generateRandomGameName() {
        Random rand = new Random();
        return  maleAndjectives[rand.nextInt(maleAndjectives.length)] + " " + maleNouns[rand.nextInt(maleNouns.length)];
    }
}
