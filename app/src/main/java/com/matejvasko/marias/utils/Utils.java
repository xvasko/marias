package com.matejvasko.marias.utils;

import android.content.SharedPreferences;
import android.content.res.Resources;

import com.matejvasko.marias.R;

public class Utils {

    public static int getStatusBarHeight(Resources resources) {
        int result = 0;
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getCurrentBackground(Resources resources, SharedPreferences sharedPref) {
        String activeSquareID;
        try {
            activeSquareID = resources.getResourceEntryName(sharedPref.getInt("activeBackground", 0));
        } catch (Resources.NotFoundException e) {
            activeSquareID = "0";
        }

        if (activeSquareID.contains("0")) {
            return R.color.shopBackground0;
        } else if (activeSquareID.contains("1")) {
            return R.color.shopBackground1;
        } else if (activeSquareID.contains("2")) {
            return R.color.shopBackground2;
        } else if (activeSquareID.contains("3")) {
            return R.color.shopBackground3;
        } else if (activeSquareID.contains("4")) {
            return R.color.shopBackground4;
        } else if (activeSquareID.contains("5")) {
            return R.color.shopBackground5;
        } else if (activeSquareID.contains("6")) {
            return R.color.shopBackground6;
        } else {
            return R.color.shopBackground7;
        }
    }

    public static int getCurrentRedAvatar(SharedPreferences sharedPref) {
        if (sharedPref.getBoolean("active0Avatar", false)) {
            return R.drawable.person_red_icon_mustache;
        } else {
            return R.drawable.person_red_icon;
        }
    }

    public static int getCurrentBlueAvatar(SharedPreferences sharedPref) {
        if (sharedPref.getBoolean("active1Avatar", false)) {
            return R.drawable.person_blue_icon_cigare;
        } else {
            return R.drawable.person_blue_icon;
        }
    }

}
