package com.matejvasko.marias.utils;

import android.content.Context;

import com.matejvasko.marias.enums.Color;
import com.matejvasko.marias.enums.GameStatus;
import com.matejvasko.marias.models.Game;
import com.matejvasko.marias.models.GameState;
import com.matejvasko.marias.models.Player;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;


public class RealmUtils {

    private Realm realm;

    public RealmUtils(Context context) {
        Realm.init(context);
        realm = Realm.getDefaultInstance();
    }

    public void createGame(String gameName, boolean isQuadrupleGame) {
        realm.beginTransaction();
        Game game = realm.createObject(Game.class);
        game.setName(gameName);
        game.setQuadrupleGame(isQuadrupleGame);
        game.setStatus(GameStatus.CREATING.getValue());
        game.setDate(DateUtils.getDate());
        realm.commitTransaction();
    }

    public boolean deleteGame(String gameName) {
        realm.beginTransaction();
        RealmResults<Game> result = realm.where(Game.class)
                .equalTo("name", gameName)
                .findAll();
        boolean wasSuccessful = result.deleteAllFromRealm();
        realm.commitTransaction();
        return wasSuccessful;
    }

    public void deleteGameHistory(String gameName, int from) {
        Game game = findGame(gameName);
        RealmList<GameState> gameHistory = game.getGameHistory();
        int gameHistorySize = gameHistory.size();
        realm.beginTransaction();
        for (int i = from; i < gameHistorySize; i++) {
            gameHistory.remove(gameHistory.size() - 1);
        }
        realm.commitTransaction();
    }

    public void persistGameState(Game game, CircularArrayList<Player> players) {
        realm.beginTransaction();
        GameState gameState = realm.createObject(GameState.class);
        gameState.setPlayer1Amount(players.get(0).getAmount().toString());
        gameState.setPlayer2Amount(players.get(1).getAmount().toString());
        gameState.setPlayer3Amount(players.get(2).getAmount().toString());
        if (game.isQuadrupleGame()) {
            gameState.setPlayer4Amount(players.get(3).getAmount().toString());
        }
        game.getGameHistory().add(gameState);
        realm.commitTransaction();
    }

    public void updateGameStatus(Game game, CircularArrayList<Player> players) {
        realm.beginTransaction();
        game.setStatus(GameStatus.IN_PROGRESS.getValue());
        for (int i = 0; i < players.size(); i++) {
            Color initialPlayersColor;
            if (game.isQuadrupleGame()) {
                initialPlayersColor = Color.BLACK;
            } else {
                initialPlayersColor = Color.BLUE;
            }
            if (players.get(i).getColor().equals(initialPlayersColor.getValue())) {
                game.setInitialPlayer(i);
            }
        }
        realm.commitTransaction();
    }

    public Game findGame(String gameName) {
        RealmResults<Game> result = realm.where(Game.class)
                .equalTo("name", gameName)
                .findAll();
        if (result.size() == 0) {
            return null;
        }
        return result.get(0);
    }

    public RealmResults<Game> findAllGames() {
        return realm.where(Game.class)
                .sort("name", Sort.ASCENDING)
                .findAll();
    }

    public void drop() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(GameState.class);
                realm.delete(Game.class);
            }
        });
    }

}
