package com.matejvasko.marias.models;

import io.realm.RealmObject;

public class GameState extends RealmObject {
    String player1Amount;
    String player2Amount;
    String player3Amount;
    String player4Amount;

    String player1Color;
    String player2Color;
    String player3Color;
    String player4Color;

    public String getPlayer1Amount() {
        return player1Amount;
    }

    public void setPlayer1Amount(String player1Amount) {
        this.player1Amount = player1Amount;
    }

    public String getPlayer2Amount() {
        return player2Amount;
    }

    public void setPlayer2Amount(String player2Amount) {
        this.player2Amount = player2Amount;
    }

    public String getPlayer3Amount() {
        return player3Amount;
    }

    public void setPlayer3Amount(String player3Amount) {
        this.player3Amount = player3Amount;
    }

    public String getPlayer4Amount() {
        return player4Amount;
    }

    public void setPlayer4Amount(String player4Amount) {
        this.player4Amount = player4Amount;
    }

    public String getPlayer1Color() {
        return player1Color;
    }

    public void setPlayer1Color(String player1Color) {
        this.player1Color = player1Color;
    }

    public String getPlayer2Color() {
        return player2Color;
    }

    public void setPlayer2Color(String player2Color) {
        this.player2Color = player2Color;
    }

    public String getPlayer3Color() {
        return player3Color;
    }

    public void setPlayer3Color(String player3Color) {
        this.player3Color = player3Color;
    }

    public String getPlayer4Color() {
        return player4Color;
    }

    public void setPlayer4Color(String player4Color) {
        this.player4Color = player4Color;
    }
}
