package com.matejvasko.marias.models;

import java.math.BigDecimal;

public class Player {
    private BigDecimal amount;
    private String color;

    public Player() {
        amount = new BigDecimal("0.00");
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
