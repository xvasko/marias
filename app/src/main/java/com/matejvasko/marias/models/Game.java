package com.matejvasko.marias.models;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Game extends RealmObject {

    private String name;
    private boolean isQuadrupleGame;
    private String status;
    private String date;
    private int initialPlayer;
    private RealmList<GameState> gameHistory = new RealmList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isQuadrupleGame() {
        return isQuadrupleGame;
    }

    public void setQuadrupleGame(boolean quadrupleGame) {
        isQuadrupleGame = quadrupleGame;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getInitialPlayer() {
        return initialPlayer;
    }

    public void setInitialPlayer(int initialBlackPlayer) {
        this.initialPlayer = initialBlackPlayer;
    }

    public RealmList<GameState> getGameHistory() {
        return gameHistory;
    }

    public void setGameHistory(RealmList<GameState> gameHistory) {
        this.gameHistory = gameHistory;
    }
}
